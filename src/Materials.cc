#include "G4Material.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "Materials.hh"

Materials::Materials()
{
  Initialise();
}
Materials::~Materials()
{
}

void Materials::Initialise()
{
  G4String name, symbol;
  G4double a, z;
  G4int ncomp;
  G4double fractionmass;
   
  a = 1.01*g/mole;
  G4Element* elH  = new G4Element(name="Hydrogen",symbol="H" , z= 1., a);

  a = 12.01*g/mole;
  G4Element* elC = new G4Element(name="Carbon", symbol="C", z=6., a);

  a = 16.00*g/mole;
  G4Element* elO  = new G4Element(name="Oxygen"  ,symbol="O" , z= 8., a);

  a = 14.01*g/mole;
  G4Element* elN  = new G4Element(name="Nitrogen",symbol="N" , z= 7., a);

  G4Material* Epoxy = new G4Material("Epoxy", 1.3 *g/cm3, 3);
  Epoxy->AddElement(elH, 44);
  Epoxy->AddElement(elC, 15);
  Epoxy->AddElement(elO, 7);

  G4Material* CarbonFiber = new G4Material("CarbonFiber", 1.5 *g/cm3, ncomp=3);
  CarbonFiber->AddElement(elC, fractionmass = 0.65*perCent);
  CarbonFiber->AddMaterial(Epoxy, fractionmass = 0.35*perCent);

  G4Material* Kapton = new G4Material("Kapton", 1.43 *g/cm3, ncomp=4);
  Kapton->AddElement(elO,5);
  Kapton->AddElement(elC,22);
  Kapton->AddElement(elN,2);
  Kapton->AddElement(elH,10);

}

G4Material* Materials::GetMaterial(const G4String& name)
{
  G4Material* ma = G4Material::GetMaterial(name);
  G4cout << "Material is selected: " << ma->GetName() << G4endl;
  return ma;
}
