
/// \file MyPhysicsList.cc
/// \brief Implementation of the MyPhysicsList class

#include "B2aPhysicsList.hh"
#include "G4DecayPhysics.hh"
#include "G4EmLivermorePhysics.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B2aPhysicsList::B2aPhysicsList()
: G4VModularPhysicsList(){
  SetVerboseLevel(0);

  // 设置低能电磁过程
  RegisterPhysics(new G4EmLivermorePhysics());

 }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B2aPhysicsList::~B2aPhysicsList()
{ 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B2aPhysicsList::SetCuts()
{
  G4VUserPhysicsList::SetCuts();
} 
// Default physics
//RegisterPhysics(new G4DecayPhysics()); 
// Radioactive decay
//RegisterPhysics(new G4RadioactiveDecayPhysics());
