//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file B2aDetectorConstruction.cc
/// \brief Implementation of the B2aDetectorConstruction class
 
#include "B2aDetectorConstruction.hh"
#include "B2aDetectorMessenger.hh"
#include "B2TrackerSD.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4SDManager.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4GlobalMagFieldMessenger.hh"
#include "G4AutoDelete.hh"

#include "G4GeometryTolerance.hh"
#include "G4GeometryManager.hh"

#include "G4UserLimits.hh"

#include "Materials.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
G4ThreadLocal 
G4GlobalMagFieldMessenger* B2aDetectorConstruction::fMagFieldMessenger = 0;

B2aDetectorConstruction::B2aDetectorConstruction()
:G4VUserDetectorConstruction(), 
 fNbOfChambers(0),
 fLogicTarget(NULL), fLogicChamber1(NULL), fLogicChamber2(NULL),
 fTargetMaterial(NULL), fChamberMaterial(NULL), 
 fStepLimit(NULL),
 fCheckOverlaps(true)
{
  fMessenger = new B2aDetectorMessenger(this);

  fNbOfChambers = 6;
  fLogicChamber1 = new G4LogicalVolume*[fNbOfChambers];
  fLogicChamber2 = new G4LogicalVolume*[fNbOfChambers];
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
B2aDetectorConstruction::~B2aDetectorConstruction()
{
  delete [] fLogicChamber1;
  delete [] fLogicChamber2;
  delete fStepLimit;
  delete fMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
G4VPhysicalVolume* B2aDetectorConstruction::Construct()
{
  // Define materials
  DefineMaterials();

  // Define volumes
  return DefineVolumes();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B2aDetectorConstruction::DefineMaterials()
{
  // Material definition 

  G4NistManager* nistManager = G4NistManager::Instance();

  // Air defined using NIST Manager
  nistManager->FindOrBuildMaterial("G4_AIR");
  
  // Lead defined using NIST Manager
  fTargetMaterial  = nistManager->FindOrBuildMaterial("G4_Pb");

  // Xenon gas defined using NIST Manager
  fChamberMaterial = nistManager->FindOrBuildMaterial("G4_Si");

  flex3Material = nistManager->FindOrBuildMaterial("G4_Al");

  G4String name, symbol;
  G4double a, z;
  G4int ncomp;
  G4double fractionmass, density;


  G4Element* elH  = new G4Element(name="Hydrogen",symbol="H" , z= 1., a = 1.01*g/mole);

  G4Element* elC = new G4Element(name="Carbon", symbol="C", z=6., a = 12.01*g/mole);

  G4Element* elO  = new G4Element(name="Oxygen"  ,symbol="O" , z= 8., a = 16.00*g/mole);

  G4Element* elN  = new G4Element(name="Nitrogen",symbol="N" , z= 7., a = 14.01*g/mole);

  G4Material* Epoxy = new G4Material("Epoxy", density = 1.3*g/cm3, ncomp=3);
  Epoxy->AddElement(elH, 44);
  Epoxy->AddElement(elC, 15);
  Epoxy->AddElement(elO, 7);

  G4Material* CarbonFiber = new G4Material("CarbonFiber", density = 1.5*g/cm3, ncomp=2);
  CarbonFiber->AddElement(elC, fractionmass = 0.65);
  CarbonFiber->AddMaterial(Epoxy, fractionmass = 0.35);

  G4Material* Kapton = new G4Material("Kapton", density = 1.43*g/cm3, ncomp=4);
  G4int natoms;
  Kapton->AddElement(elO,natoms=5);
  Kapton->AddElement(elC,natoms=22);
  Kapton->AddElement(elN,natoms=2);
  Kapton->AddElement(elH,natoms=10);

  // Print materials
  G4cout << *(G4Material::GetMaterialTable()) << G4endl;
  G4double radl = Epoxy->GetRadlen();
  G4cout << "radiation length: " << radl << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* B2aDetectorConstruction::DefineVolumes()
{
  G4Material* air  = G4Material::GetMaterial("G4_AIR");
  G4Material* Mar_CarbonFiber = G4Material::GetMaterial("CarbonFiber");
  G4Material* Mar_Epoxy = G4Material::GetMaterial("Epoxy");
  G4Material* Mar_Kapton= G4Material::GetMaterial("Kapton");
  // Sizes of the principal geometrical components (solids)
  
  G4double chamberSpacing = 80*cm; // from chamber center to center!

  G4double chamberWidth = 20.0*cm; // width of the chambers
  G4double targetLength =  5.0*cm; // full length of Target
  
  G4double trackerLength = (fNbOfChambers+1)*chamberSpacing;

  //G4double worldLength = 1.2 * (2*targetLength + trackerLength);
  G4double worldLength = 16*cm;

  G4double targetRadius  = 0.5*targetLength;   // Radius of Target
  targetLength = 0.5*targetLength;             // Half length of the Target  
  G4double trackerSize   = 0.5*trackerLength;  // Half length of the Tracker

  // Definitions of Solids, Logical Volumes, Physical Volumes

  // World

  G4GeometryManager::GetInstance()->SetWorldMaximumExtent(worldLength);

  G4cout << "Computed tolerance = "
         << G4GeometryTolerance::GetInstance()->GetSurfaceTolerance()/mm
         << " mm" << G4endl;


  G4Box* worldS
    = new G4Box("world",                                    //its name
                worldLength/2,worldLength/2,worldLength/2); //its size
  G4LogicalVolume* worldLV
    = new G4LogicalVolume(
                 worldS,   //its solid
                 air,      //its material
                 "World"); //its name
  
  //  Must place the World Physical volume unrotated at (0,0,0).
  // 
  G4VPhysicalVolume* worldPV
    = new G4PVPlacement(
                 0,               // no rotation
                 G4ThreeVector(), // at (0,0,0)
                 worldLV,         // its logical volume
                 "World",         // its name
                 0,               // its mother  volume
                 false,           // no boolean operations
                 0,               // copy number
                 fCheckOverlaps); // checking overlaps 

  // Tracker


  G4Box* fiber
    = new G4Box("fiber", 25.6*mm/2, 12.8*mm/2, 250*um/2);
  G4LogicalVolume* FiberLV
    = new G4LogicalVolume(fiber, Mar_CarbonFiber, "Fiber",0,0,0);



  G4Box* flex1
    = new G4Box("flex1", 25.6*mm/2, 12.8*mm/2, 60*um/2);
  G4LogicalVolume* Flex1LV
    = new G4LogicalVolume(flex1, Mar_Epoxy, "Flex1", 0,0,0);



  G4Box* flex2
    = new G4Box("flex2", 25.6*mm/2, 12.8*mm/2, 74*um/2);
  G4LogicalVolume* Flex2LV
    = new G4LogicalVolume(flex2, Mar_Kapton, "Flex2", 0,0,0);


  G4Box* flex3
    = new G4Box("flex3", 25.6*mm/2, 12.8*mm/2, 26.8*um/2);
  G4LogicalVolume* Flex3LV
    = new G4LogicalVolume(flex3, flex3Material, "Flex3", 0,0,0);

  G4Box* chip
    = new G4Box("chip", 25.6*mm/2, 12.8*mm/2, 50*um/2);
  G4LogicalVolume* chipLV
    = new G4LogicalVolume(chip, fChamberMaterial, "Chip", 0,0,0);



    ////////////////////////////////
  G4VisAttributes* boxVisAtt= new G4VisAttributes(G4Colour(1.0,1.0,1.0));
  G4VisAttributes* chamberVisAtt = new G4VisAttributes(G4Colour(1.0,1.0,0.0));
  G4double deltaZ;
  G4double deltaX = 0;
  G4double deltaY = 0;
  G4int No;
  for (G4int copyNo=0; copyNo<fNbOfChambers; copyNo++) {
    fLogicChamber1[copyNo] = chipLV;
    fLogicChamber1[copyNo]->SetVisAttributes(chamberVisAtt);

    fLogicChamber2[copyNo] = chipLV;
    fLogicChamber2[copyNo]->SetVisAttributes(chamberVisAtt);

    if (copyNo == 0)
    {
      deltaZ = -57.7*mm;

    }
    else if (copyNo == 1)
    {
      deltaZ = -57.7*mm+20.8*mm;
    }

    else if (copyNo == 2)
    {
      deltaZ = -57.7*mm+20.8*mm+19.5*mm;
      deltaY = 1*mm;
    }

    else if (copyNo == 3)
    {
      deltaZ = -57.7*mm+20.8*mm+19.5*mm+34.8*mm;
    }

    else if (copyNo == 4)
    {
      deltaZ = -57.7*mm+20.8*mm+19.5*mm+34.8*mm+19.5*mm;
      deltaX = 0.5*mm;
    }
    else if (copyNo == 5)
    {
      deltaZ = -57.7*mm+20.8*mm+19.5*mm+34.8*mm+19.5*mm+20.8*mm;
    }

    No = copyNo*2;

    new G4PVPlacement(0,                            // no rotation
                      G4ThreeVector(deltaX,deltaY,-280.4*um+deltaZ), // at (x,y,z)
                      chipLV,        // its logical volume
                      "Chip",                 // its name
                      worldLV,                    // its mother  volume
                      false,                        // no boolean operations
                      No,                       // copy number
                      fCheckOverlaps);              // checking overlaps

    new G4PVPlacement(0,               // no rotation
                    G4ThreeVector(deltaX,deltaY,-225.4*um+deltaZ), // at (x,y,z) 124.6  -225.4
                    Flex1LV,       // its logical volume
                    "Flex1",       // its name
                    worldLV,         // its mother  volume
                    false,           // no boolean operations
                    copyNo,               // copy number
                    fCheckOverlaps); // checking overlaps

    new G4PVPlacement(0,               // no rotation
                    G4ThreeVector(deltaX,deltaY,-182*um+deltaZ), // at (x,y,z) 242    -108
                    Flex3LV,       // its logical volume
                    "Flex3",       // its name
                    worldLV,         // its mother  volume
                    false,           // no boolean operations
                    copyNo,               // copy number
                    fCheckOverlaps); // checking overlaps

    new G4PVPlacement(0,               // no rotation
                    G4ThreeVector(deltaX,deltaY,-131.6*um+deltaZ), // at (x,y,z)  191.6  -158.4
                    Flex2LV,       // its logical volume
                    "Flex2",       // its name
                    worldLV,         // its mother  volume
                    false,           // no boolean operations
                    copyNo,               // copy number
                    fCheckOverlaps); // checking overlaps



    new G4PVPlacement(0,               // no rotation
                    G4ThreeVector(deltaX,deltaY,-64.6*um+deltaZ), // at (x,y,z) 124.6  -225.4
                    Flex1LV,       // its logical volume
                    "Flex1",       // its name
                    worldLV,         // its mother  volume
                    false,           // no boolean operations
                    copyNo,               // copy number
                    fCheckOverlaps); // checking overlaps

    new G4PVPlacement(0,               // no rotation
                    G4ThreeVector(deltaX,deltaY,90.4*um+deltaZ), // at (x,y,z)
                    FiberLV,       // its logical volume
                    "Fiber",       // its name
                    worldLV,         // its mother  volume
                    false,           // no boolean operations
                    copyNo,               // copy number
                    fCheckOverlaps); // checking overlaps

        new G4PVPlacement(0,               // no rotation
                    G4ThreeVector(deltaX,deltaY,245.4*um+deltaZ), // at (x,y,z) 124.6  -225.4
                    Flex1LV,       // its logical volume
                    "Flex1",       // its name
                    worldLV,         // its mother  volume
                    false,           // no boolean operations
                    copyNo,               // copy number
                    fCheckOverlaps); // checking overlaps

    new G4PVPlacement(0,               // no rotation
                    G4ThreeVector(deltaX,deltaY,312.4*um+deltaZ), // at (x,y,z)  191.6  -158.4
                    Flex2LV,       // its logical volume
                    "Flex2",       // its name
                    worldLV,         // its mother  volume
                    false,           // no boolean operations
                    copyNo,               // copy number
                    fCheckOverlaps); // checking overlaps

    new G4PVPlacement(0,               // no rotation
                    G4ThreeVector(deltaX,deltaY,362.8*um+deltaZ), // at (x,y,z) 242    -108
                    Flex3LV,       // its logical volume
                    "Flex3",       // its name
                    worldLV,         // its mother  volume
                    false,           // no boolean operations
                    copyNo,               // copy number
                    fCheckOverlaps); // checking overlaps



    new G4PVPlacement(0,               // no rotation
                    G4ThreeVector(deltaX,deltaY, 406.2*um+deltaZ), // at (x,y,z) 124.6  -225.4
                    Flex1LV,       // its logical volume
                    "Flex1",       // its name
                    worldLV,         // its mother  volume
                    false,           // no boolean operations
                    copyNo,               // copy number
                    fCheckOverlaps); // checking overlaps

    new G4PVPlacement(0,                            // no rotation
                      G4ThreeVector(deltaX,deltaY, 461.2*um+deltaZ), // at (x,y,z)   -69.6
                      chipLV,        // its logical volume
                      "Chip",                 // its name
                      worldLV,                    // its mother  volume
                      false,                        // no boolean operations
                      No+1,                       // copy number
                      fCheckOverlaps);              // checking overlaps



}


  // Visualization attributes



  worldLV      ->SetVisAttributes(boxVisAtt);
  //fLogicTarget ->SetVisAttributes(boxVisAtt);
  //trackerLV    ->SetVisAttributes(boxVisAtt);


  //G4Box* chamberS
    //= new G4Box("Chamber_solid", 25.6*mm, 12.8*mm, 50*um);
  //G4LogicalVolume* ChamberLV = new G4LogicalVolume(chamberS,fChamberMaterial,"Chamber_LV",0,0,0);

  /*
  for (G4int copyNo=0; copyNo<fNbOfChambers; copyNo++) {

      G4double Zposition = -2*cm + copyNo * 1*cm;
      G4double rmax =  rmaxFirst + copyNo * rmaxIncr;

      //G4Box* chamberS
        //= new G4Box("Chamber_solid", 25.6*mm, 12.8*mm, 50*um);
      //G4Material* fChamberMaterial = nist->FindOrBuildMaterial("G4_Si");

      fLogicChamber[copyNo] = trackerLV;
              //new G4LogicalVolume(chamberS,fChamberMaterial,"Chamber_LV",0,0,0);

      fLogicChamber[copyNo]->SetVisAttributes(chamberVisAtt);

      new G4PVPlacement(0,                            // no rotation
                        G4ThreeVector(0,0,Zposition), // at (x,y,z)
                        fLogicChamber[copyNo],        // its logical volume
                        "Tracker_LV",                 // its name
                        worldLV,                    // its mother  volume
                        false,                        // no boolean operations
                        copyNo,                       // copy number
                        fCheckOverlaps);              // checking overlaps 

  }
  */

  // Example of User Limits
  //
  // Below is an example of how to set tracking constraints in a given
  // logical volume
  //
  // Sets a max step length in the tracker region, with G4StepLimiter

  G4double maxStep = 0.5*50*um;
  fStepLimit = new G4UserLimits(maxStep);
  //trackerLV->SetUserLimits(fStepLimit);
 
  /// Set additional contraints on the track, with G4UserSpecialCuts
  ///
  /// G4double maxLength = 2*trackerLength, maxTime = 0.1*ns, minEkin = 10*MeV;
  /// trackerLV->SetUserLimits(new G4UserLimits(maxStep,
  ///                                           maxLength,
  ///                                           maxTime,
  ///                                           minEkin));

  // Always return the physical world

  return worldPV;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
void B2aDetectorConstruction::ConstructSDandField()
{
  // Sensitive detectors

  G4String trackerChamberSDname = "B2/TrackerChamberSD";
  B2TrackerSD* aTrackerSD = new B2TrackerSD(trackerChamberSDname,
                                            "TrackerHitsCollection");
  G4SDManager::GetSDMpointer()->AddNewDetector(aTrackerSD);
  // Setting aTrackerSD to all logical volumes with the same name 
  // of "Chamber_LV".
  SetSensitiveDetector("Chip", aTrackerSD, true);

  // Create global magnetic field messenger.
  // Uniform magnetic field is then created automatically if
  // the field value is not zero.
  //G4ThreeVector fieldValue = G4ThreeVector();
  //fMagFieldMessenger = new G4GlobalMagFieldMessenger(fieldValue);
  //fMagFieldMessenger->SetVerboseLevel(1);
  
  // Register the field messenger for deleting
  //G4AutoDelete::Register(fMagFieldMessenger);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
void B2aDetectorConstruction::SetTargetMaterial(G4String materialName)
{
  G4NistManager* nistManager = G4NistManager::Instance();

  G4Material* pttoMaterial = 
              nistManager->FindOrBuildMaterial(materialName);

  if (fTargetMaterial != pttoMaterial) {
     if ( pttoMaterial ) {
        fTargetMaterial = pttoMaterial;
        if (fLogicTarget) fLogicTarget->SetMaterial(fTargetMaterial);
        G4cout 
          << G4endl 
          << "----> The target is made of " << materialName << G4endl;
     } else {
        G4cout 
          << G4endl 
          << "-->  WARNING from SetTargetMaterial : "
          << materialName << " not found" << G4endl;
     }
  }
}
 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


void B2aDetectorConstruction::SetChamberMaterial(G4String materialName)
{
  G4NistManager* nistManager = G4NistManager::Instance();

  G4Material* pttoMaterial =
              nistManager->FindOrBuildMaterial(materialName);

  if (fChamberMaterial != pttoMaterial) {
     if ( pttoMaterial ) {
        fChamberMaterial = pttoMaterial;
        for (G4int copyNo=0; copyNo<fNbOfChambers; copyNo++) {
            if (fLogicChamber1[copyNo]) fLogicChamber1[copyNo]->
                                               SetMaterial(fChamberMaterial);
        }
        G4cout 
          << G4endl 
          << "----> The chambers are made of " << materialName << G4endl;
     } else {
        G4cout 
          << G4endl 
          << "-->  WARNING from SetChamberMaterial : "
          << materialName << " not found" << G4endl;
     }
  }
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B2aDetectorConstruction::SetMaxStep(G4double maxStep)
{
  if ((fStepLimit)&&(maxStep>0.)) fStepLimit->SetMaxAllowedStep(maxStep);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B2aDetectorConstruction::SetCheckOverlaps(G4bool checkOverlaps)
{
  fCheckOverlaps = checkOverlaps;
}  
