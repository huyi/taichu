//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file B2RunAction.cc
/// \brief Implementation of the B2RunAction class

#include "B2RunAction.hh"
#include "B2TrackerSD.hh"
#include "B2aAnalysis.hh"
#include "G4Run.hh"
#include "G4RunManager.hh"



//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B2RunAction::B2RunAction(B2TrackerSD *fB2TrackerSD)
 : G4UserRunAction(),fB2TrackerSD(fB2TrackerSD)
{ 
  // set printing event number per each 100 events
  G4RunManager::GetRunManager()->SetPrintProgress(1);
  auto analysisManager = G4AnalysisManager::Instance();
  //G4cout << "Using " << analysisManager->GetType() << G4endl;
  analysisManager->SetDefaultFileType("root");
  analysisManager->SetVerboseLevel(1);
  analysisManager->SetNtupleMerging(true);
  analysisManager->SetFileName("B2");

  analysisManager->CreateNtuple("B2", "Hits");
  //analysisManager->CreateNtupleIColumn("evtID");
  //analysisManager->CreateNtupleIColumn("numOfHits");
  //analysisManager->CreateNtupleIColumn("trcID", trackID);

  if(fB2TrackerSD){
    analysisManager->CreateNtupleIColumn("EventID");
    analysisManager->CreateNtupleIColumn("trcID",fB2TrackerSD->GettrackID());
    analysisManager->CreateNtupleIColumn("planeID",fB2TrackerSD->GetplaneID());
    analysisManager->CreateNtupleDColumn("Energy_deposition",fB2TrackerSD->GetEp());  //MeV
    analysisManager->CreateNtupleDColumn("x",fB2TrackerSD->Getx());  // mm
    analysisManager->CreateNtupleDColumn("y",fB2TrackerSD->Gety());
    analysisManager->CreateNtupleDColumn("z",fB2TrackerSD->Getz());
    analysisManager->FinishNtuple();
  }
  



  
  analysisManager->SetNtupleFileName(0, "B2ntuple");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B2RunAction::~B2RunAction()
{
  delete G4AnalysisManager::Instance();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B2RunAction::BeginOfRunAction(const G4Run*)
{ 
  //inform the runManager to save random number seed
  //G4RunManager::GetRunManager()->SetRandomNumberStore(false);
  //auto analysisManager = G4AnalysisManager::Instance();
  auto analysisManager = G4AnalysisManager::Instance();

  // Open an output file
  //
  analysisManager->OpenFile();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B2RunAction::EndOfRunAction(const G4Run* )
{


  auto analysisManager = G4AnalysisManager::Instance();
  analysisManager->Write();
  analysisManager->CloseFile();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
