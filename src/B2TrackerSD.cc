//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file B2TrackerSD.cc
/// \brief Implementation of the B2TrackerSD class
//#include <TH1D.h>
#include "B2aAnalysis.hh"
#include "B2TrackerSD.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"
#include "B2RunAction.hh"
#include "G4UnitsTable.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B2TrackerSD::B2TrackerSD(const G4String& name,
                         const G4String& hitsCollectionName) 
 : G4VSensitiveDetector(name),
   fHitsCollection(NULL)
{
  collectionName.insert(hitsCollectionName);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B2TrackerSD::~B2TrackerSD() 
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B2TrackerSD::Initialize(G4HCofThisEvent* hce)
{
  // Create hits collection

  fHitsCollection 
    = new B2TrackerHitsCollection(SensitiveDetectorName, collectionName[0]); 

  // Add this collection in hce

  G4int hcID 
    = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
  hce->AddHitsCollection( hcID, fHitsCollection ); 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool B2TrackerSD::ProcessHits(G4Step* aStep, 
                                     G4TouchableHistory*)
{  
  // energy deposit
  G4double edep = aStep->GetTotalEnergyDeposit();

  if (edep==0.) return false;

  B2TrackerHit* newHit = new B2TrackerHit();

  //newHit->SetEventID  (aStep->GetTrack()->GetTrackID());

  newHit->SetTrackID  (aStep->GetTrack()->GetTrackID());
  newHit->SetChamberNb(aStep->GetPreStepPoint()->GetTouchableHandle()
                                               ->GetCopyNumber());
  newHit->SetEdep(edep);
  newHit->SetPos (aStep->GetPostStepPoint()->GetPosition());

  fHitsCollection->insert( newHit );

  //newHit->Print();

  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B2TrackerSD::EndOfEvent(G4HCofThisEvent*)
{
  //if ( verboseLevel>1 ) {
     G4int nofHits = fHitsCollection->entries();
     numEvent  += 1;
     G4cout << G4endl
            << "-------->Hits Collection: in this event there are " << nofHits
            << " hits in total detector: " << G4endl;
     auto analysisManager = G4AnalysisManager::Instance();
     trackID.clear();
     planeID.clear();
     Ep.clear();
     x.clear();
     y.clear();
     z.clear();
     for ( G4int i=0; i<nofHits; i++ )
    {
      //(*fHitsCollection)[i]->Print();
      trackID.push_back((*fHitsCollection)[i]->GetTrackID());
      planeID.push_back((*fHitsCollection)[i]->GetChamberNb());

      Ep.push_back((*fHitsCollection)[i]->GetEdep());
      x.push_back((*fHitsCollection)[i]->GetPos().x());
      y.push_back((*fHitsCollection)[i]->GetPos().y());
      z.push_back((*fHitsCollection)[i]->GetPos().z());
      //if (trackID.at(i) == 1){
      //analysisManager->FillNtupleIColumn(0, 1, numEvent);

      

      //}
      //G4cout <<"aaajsdejhjfhj!!!!!!!" <<trackID.at(i)<<G4endl;
      //analysisManager->FillNtupleDColumn(0, i, trackID.at(i));

    }
      //analysisManager->FillNtupleIColumn(0, 0, trackID);
      //analysisManager->FillNtupleIColumn(0, 1, planeID);
      //analysisManager->FillNtupleDColumn(0, 2, Ep);
      //analysisManager->FillNtupleDColumn(0, 3, x);
      //analysisManager->FillNtupleDColumn(0, 4, y);
      //analysisManager->FillNtupleDColumn(0, 5, z);
      analysisManager->FillNtupleIColumn(0, 0, numEvent);
      analysisManager->AddNtupleRow();

    //analysisManager->AddNtupleRow();
    //analysisManager->FillNtupleIColumn(0, 0, trackID.at(i));
    //analysisManager->FillNtupleIColumn(1, trackID);
    //analysisManager->AddNtupleRow();
  //}

     //G4cout <<"aaajsdejhjfhj!!!!!!!" <<trackID.at(0)<<G4endl;

         //auto analysisManager = G4AnalysisManager::Instance();



  //analysisManager->AddNtupleRow(0);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
