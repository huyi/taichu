
/// \file B3PhysicsList.hh
/// \brief Definition of the B3PhysicsList class

#ifndef B2aPhysicsList_h
#define B2aPhysicsList_h 1

#include "G4VModularPhysicsList.hh"

/// Modular physics list
///
/// It includes the folowing physics builders
/// - G4DecayPhysics
/// - G4RadioactiveDecayPhysics
/// - G4EmStandardPhysics

class B2aPhysicsList: public G4VModularPhysicsList
{
public:
  B2aPhysicsList();
  virtual ~B2aPhysicsList();

  virtual void SetCuts();
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

