#ifndef MATERIALS_H
#define MATERIALS_H

#include "globals.hh"
class G4Material;
class Materials {
public:
  Materials();
  ~Materials();
  G4Material* GetMaterial(const G4String&);    
private:
  void Initialise();
};

#endif

